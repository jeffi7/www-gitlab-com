---
layout: handbook-page-toc
title: Ops Hiring Process
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

We are running with some limited capacity in the recruiting team and we are asking hiring managers to cover all steps in the hiring process.

## Hiring Process

The process for Ops is a mix of the current [hiring manager (HM) tasks](/handbook/hiring/talent-acquisition-framework/hiring-manager/) and the [talent acquisition (TA) tasks](/handbook/hiring/talent-acquisition-framework/req-overview/). 

### Step 1. Identify hiring need

[Identify hiring need](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-1hm-identifying-hiring-need)

1. Work with Product Manager as they are [DRI of headcount planning](/handbook/engineering/#headcount-planning), Finance, and Talent Acquisition to [include vacancy in the hiring plan](/handbook/hiring/talent-acquisition-framework/req-creation/#adding-vacancies-to-the-hiring-plan-dri-hiring-manager). You should obtain a GitLab Hiring Plan ID (GHP ID)
1. [Create or Review the Job Family](/handbook/hiring/talent-acquisition-framework/req-creation/#create-or-review-the-job-family)

### Step 2. Kickoff

1. [Create Kickoff](/handbook/hiring/talent-acquisition-framework/req-overview/#step-3-complete-kick-off-session-agree-on-priority-level--complete-a-sourcing-session) 
1.  Create a separate kickoff issue for each req you are hiring (each unique GHID).  For identical roles one kickoff issue can just be a link to another, but having a separate issue for each req is valuable for tracking purposes.
1. Include label ~section::ops to the kickoff issue so we can track in [this board](https://gitlab.com/gl-talent-acquisition/req-intake/-/boards/3516477?label_name[]=section%3A%3Aops) (confidential) and use it as the single source of thruth (SSOT)
1. [Complete Kickoff](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-2hm-complete-kick-off)

### Step 3. Setup job in Greenhouse
1. Work with TA to [open the vacancy in Greenhouse](/handbook/hiring/talent-acquisition-framework/req-creation/#opening-vacancies-in-greenhouse-dri-recruiter)
1. [Setup scoreboard](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-3hm-setup-scorecard-and-prepare-interview-team)
1. Publish job ad

### Step 4. Sourcing 

The main purpose of sourcing is to **Identify and engage top talent**

1. Get a [recruiter LinkedIn account](/handbook/hiring/sourcing/#upgrading-your-linkedin-account) and setup 
1. Setup LinkedIn for sourcing, for example:
    1. Create your own templates
    1. Create projects for each of your open positions or make sure you are invited to
1. Search for candidates
    1. If you are getting support from a sourcer, follow the steps to [review inbound applications](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-4hm-optional-source-candidates-andor-review-inbound-applications)
1. Reach Out. Consider using the templates listed in the resources. Make sure to ask for contact details of the prospect, you will need this in the next step.
1. [Add prospects to Greenhouse](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/req-overview/#step-4-indentify--engage-top-talent-prospects). In the prospect's profile, click in the three dots, and click on export to ATS. 
1. After importing to Greenhouse, edit the profile (under Details tab, there is an edit button) to include the email and any other contact information. This is important because Greenhouse automatically sends an email to the candidate when is moved from prospect to candidate to fill a form which includes the CV to be uploaded there.

**Resources**:
- Mark Deubel's podcast: [Foundtaions - Sourcing Outreach](https://doobles.podbean.com/e/building-the-foundation-sourcing-outreach-pt1-episode-3/) and [Guest Show - Sourcing outreach](https://doobles.podbean.com/e/guest-show-sourcing-outreach-with-dov-and-mark/)
- [Talent Acquisition Reach out templates](https://docs.google.com/presentation/d/1ySqgLoYnFUGtb7hdywav6iSb_NBPRhfIs6WZlGne6Ww/edit?usp=sharing) (private)
- [Ops reach out templates](https://gitlab.com/gitlab-com/ops-sub-department/ops-hiring-process/-/blob/main/outreach-messages.md) (private)
- [Sourcing](https://about.gitlab.com/handbook/hiring/sourcing/)
- [LinkedIn training](https://docs.google.com/presentation/d/1W9PvVp2uGFsWHFTQrAd5ZjgzfAMmS_dQI6R7Lg7XDJc/edit#slide=id.g7ba34d75e8_0_16) (private)
- [LinkedIn boolean search](https://docs.google.com/spreadsheets/d/1wRGwx_GT14udxnoMOF-gP2XQaRhJA_8bEVwWwIdBio4/edit#gid=666961789) (private)
- [Diversity Boolean strings](https://docs.google.com/spreadsheets/d/1Hs3UVEpgYOJgvV8Nlyb0Cl5P6_8IlAlxeLQeXz64d8Y/edit#gid=0) (private)
- [Single Search Permutator](https://docs.google.com/spreadsheets/d/1TmO1z077y8KvN-V69xkdwX1YmekQiSW-SroDpRTVzj0/edit#gid=1892333325) (private)
- [Video training for diversity sourcing](https://drive.google.com/file/d/1UJdzcaiaud42OhMSetTa8P_wqoWofBMU/view?usp=sharing) (private)

### Step 5. Screening

1. Schedule screening calls (3-6 per week) 
1. Tag TA on the candidates Greenhouse profile so they can support with screening calls. and work with TA in case they have capacity to do the screening
1. [Screening](/handbook/hiring/talent-acquisition-framework/req-overview/#screening). Don't forget to introduce the TA to the prospect at the start of the team interview stage so they can build a relationship with the candidate and ensure a consistent experience.

### Step 6. Continuous check-in

[Continuous check-in](/handbook/hiring/talent-acquisition-framework/req-overview/#step-5-weekly-check-in-issue)

### Step 7. Assessment

[Assessment](/handbook/hiring/talent-acquisition-framework/req-overview/#assessment)

### Step 8.Schedule interviews

Ping TA to [Schedule interviews](/handbook/hiring/talent-acquisition-framework/req-overview/#team-interview) so they can manage the candidate throughout the interview stages

### Step 9. Perform interviews

### Step 10. Complete feedback

[Complete feedback](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-5hm-hiring-team-to-complete-feedback-in-greenhouse)

### Step 11. References

1. [Request references](/handbook/hiring/talent-acquisition-framework/req-overview/#step-7-references-and-background-check)
1. [Complete references](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-6hm-complete-references)

### Step 12. Justification

[Justification](/handbook/hiring/talent-acquisition-framework/req-overview/#step-8-justification-engineering-only)
