---
layout: markdown_page
title: "FY23-Q1 OKRs"
description: "View GitLabs Objective-Key Results for FY23 Q1. Learn more here!"
canonical_path: "/company/okrs/fy23-q1/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from February 1, 2022 to April 30, 2022.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2021-12-28 | CEO shares top goals with E-group for feedback |
| -4 | 2022-01-03 | CEO pushes top goals to this page |
| -4 | 2022-01-03 | E-group propose OKRs for their functions in [Epics and Issues nested under the CEO's OKRs](/company/okrs/#executives-propose-okrs-for-their-functions). These issues and epics are shared in #okrs Slack channel |
| -3 | 2022-01-10 | E-group 50 minute draft review meeting on 2021-01-10 |
| -2 | 2022-01-17 | E-group discusses with their respective teams and polish their OKRs |
| -1 | 2022-01-24 | CEO reports post links to final OKR Epics in #okrs slack channel and @ mention the CEO and CoS to the CEO for approval |
| 0  | 2022-02-01 | CoS to the CEO updates OKR page for current quarter to be active |


## OKRs

### 1. CEO

### 2. CEO

### 3. CEO


